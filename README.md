# Cloudlab Project
With this project we had different expectations closely linked to our background and our desire to improve the way we are using technologies. Recently we were working on a project using a monolithic approach for our graphQL  API. We were interrogating ourselves on how it will be possible to divide it into micro services and how to make a link between them in order to be able to exchange information between services. We thought it could be the moment to experiment and to answer our interrogations.
During the conception of our project architecture, we chose to work on the following versions:
1.	**First version:** In few words, we chose to use Docker compose in order to deploy our solution.  
2.	**Second version:** To summarize we chose to deploy our solution using Kubernetes. 

## Our configuration
For this project you will need **Minikube** and **docker-compose** installed on your machine.
## Version A
For this version only **docker-compose** is required to run our project. There are few steps you need to follow in order to be able to deploy the project using docker.

***Steps:***
 1. **Clone the repository:** Using the following command `git clone url`
 2.  **Navigate into the repository called Version A:** Using the following command `cd cloudproject/VersionA`
 3. **Deploy and Run the project:** Using the following command `docker-compose up`. It can takes some time, be patient.
 
 Now you should see some logs saying that they are pulling the images in order to run the project. When all the containers are ready you can type the following URL in your browser :

> `http://localhost:22612`

 It will displays our front working with the API gateway, the micro services and the database.

## Version B
For this version  **docker** and **Minikube** are required to run our project. There are few steps you need to follow in order to be able to deploy the project using docker.

***Steps:***
 1. **Start Minikube:** Using the following commands `minikube start && minikube dashboard`
 2. **Enable ingress addon:** Using the following command `minikube addons enable ingress`
 3. **Add a line in your /etc/hosts file:** With the result given by the `minikube ip` command you must add a line in your **/etc/hosts** file in order to enable the name resolution locally of an Ip to an hostname. Add the following line to your **/etc/hosts**: 

> minikube-ip **cloudlab.project**

 3.  **Create the folder for the persistence in your cluster:** Use the following command to connect into your minikube cluster `minikube ssh`. After this run the following command to create the folder used for the persistence `mkdir /home/docker/mongoSave`. After the repository is created used the command `exit` to leave the cluster bash.
 4. **Navigate into the repository called Version B:** Using the following command `cd cloudproject/VersionB` 
 5.  **Apply the configuration files: ** Using the following command `kubectl apply -f .` 
 
 Now you should see some logs. When all the rules are applied you can type the following URL in your 
 browser :
 > `http://cloudlab.project`

 It will displays our front working with the API gateway, the micro services and the database.

## Authors

[Tristan Clémenceau](https://gitlab.com/tristann)
[Alex Romain](https://gitlab.com/alexdan.romain)