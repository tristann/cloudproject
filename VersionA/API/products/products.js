// Open Telemetry (optional)
const { ApolloOpenTelemetry } = require('supergraph-demo-opentelemetry');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productsSchema = new Schema({
    id: { type: String, required: false },
    sku: { type: String, required: false },
    package: { type: String, required: false },
    variation: { type: String, required: false },
}, { _id: false, collection: 'products' })

const Products = mongoose.model('Products', productsSchema);

if (process.env.APOLLO_OTEL_EXPORTER_TYPE) {
    new ApolloOpenTelemetry({
        type: 'subgraph',
        name: 'products',
        exporter: {
            type: process.env.APOLLO_OTEL_EXPORTER_TYPE, // console, zipkin, collector
            host: process.env.APOLLO_OTEL_EXPORTER_HOST,
            port: process.env.APOLLO_OTEL_EXPORTER_PORT,
        }
    }).setupInstrumentation();
}

const { ApolloServer, gql } = require('apollo-server');
const { buildSubgraphSchema } = require('@apollo/subgraph');
const { readFileSync } = require('fs');

const port = process.env.APOLLO_PORT || 4000;

const products = [
    { id: 'apollo-federation', sku: 'federation', package: '@apollo/federation', variation: "OSS" },
    { id: 'apollo-studio', sku: 'studio', package: '', variation: "platform" },
]

const typeDefs = gql(readFileSync('./products.graphql', { encoding: 'utf-8' }));
const resolvers = {
    Query: {
        allProducts: async (_, args, context) => {
            return await Products.find({});
        },
        product: async (_, args, context) => {
            const res = await Products.findOne({"id" : args.id})
            return res != null ? res : new Products({
                id: "Product",
                sku: "Sku",
                package: "Package",
                variation: "Variation",
            })
        }
    },
    Product: {
        variation: (reference) => {
            if (reference.variation) return { id: reference.variation };
            return { id: products.find(p => p.id == reference.id).variation }
        },
        dimensions: () => {
            return { size: "1", weight: 1 }
        },
        createdBy: (reference) => {
            return { email: 'support@apollographql.com', totalProductsCreated: 1337 }
        },
        __resolveReference: (reference) => {
            if (reference.id) return products.find(p => p.id == reference.id);
            else if (reference.sku && reference.package) return products.find(p => p.sku == reference.sku && p.package == reference.package);
            else return { id: 'rover', package: '@apollo/rover', ...reference };
        }
    }
}
const server = new ApolloServer({ schema: buildSubgraphSchema({ typeDefs, resolvers }) });

mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@mongo:27017/${process.env.DB_NAME}`)
    .then(() => {
        console.log(`🚀 Products connected to DB`);
        server.listen({ port: port })
            .then(({ url }) => {
                console.log(`🚀 Products subgraph ready at ${url}`);
            })
            .catch(err => { console.error(err) });
    })
    .catch(err => {
        console.log(err)
    })


