// Open Telemetry (optional)
const { ApolloOpenTelemetry } = require('supergraph-demo-opentelemetry');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const inventorySchema = new Schema({
  id: { type: String, required: false },
  estimatedDelivery: { type: String, required: false },
  fastestDelivery: { type: String, required: false },
}, { _id: false, collection: 'inventory' })

const Inventory = mongoose.model('Inventory', inventorySchema);

if (process.env.APOLLO_OTEL_EXPORTER_TYPE) {
  new ApolloOpenTelemetry({
    type: 'subgraph',
    name: 'inventory',
    exporter: {
      type: process.env.APOLLO_OTEL_EXPORTER_TYPE, // console, zipkin, collector
      host: process.env.APOLLO_OTEL_EXPORTER_HOST,
      port: process.env.APOLLO_OTEL_EXPORTER_PORT,
    }
  }).setupInstrumentation();
}

const { ApolloServer, gql } = require('apollo-server');
const { buildSubgraphSchema } = require('@apollo/subgraph');
const { readFileSync } = require('fs');

const port = process.env.APOLLO_PORT;

const typeDefs = gql(readFileSync('./inventory.graphql', { encoding: 'utf-8' }));
const resolvers = {
  Product: {
    delivery: async (product, args, context) => {
      return await Inventory.findOne({ "id": product.id })
    }
  }
}
const server = new ApolloServer({ schema: buildSubgraphSchema({ typeDefs, resolvers }) });

mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@mongo:27017/${process.env.DB_NAME}`)
  .then(() => {
    console.log(`🚀 Inventory connected to DB`);
    server.listen({ port: port })
      .then(({ url }) => {
        console.log(`🚀 Inventory subgraph ready at ${url}`);
      })
      .catch(err => { console.error(err) });
  })
  .catch(err => {
    console.log(err)
  })