const { ApolloServer, gql } = require('apollo-server');
const { readFileSync } = require('fs');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pandaSchema = new Schema({
  name: { type: String, required: false },
  favoriteFood: { type: String, required: false },
}, { _id: false, collection: 'pandas' })

const Panda = mongoose.model('Panda', pandaSchema);

const port = process.env.APOLLO_PORT;

const typeDefs = gql(readFileSync('./pandas.graphql', { encoding: 'utf-8' }));
const resolvers = {
    Query: {
        allPandas: async (_, args, context) => {
            return await Panda.find({});
        },
        panda: async (_, args, context) => {
            const res = await Panda.findOne({"name" : args.name})
            return res != null ? res : new Panda({
                name: "Panda",
                favoriteFood: "Panda"
            })
        }
    },
}
const server = new ApolloServer({ typeDefs, resolvers });

mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@mongo:27017/${process.env.DB_NAME}`)
  .then(() => {
    console.log(`🚀 Pandas connected to DB`);
    server.listen( {port: port} )
    .then(({ url }) => {
        console.log(`🚀 Pandas subgraph ready at ${url}`);
      })
    .catch(err => {console.error(err)});
  })
  .catch(err => {
    console.log(err)
  })


