// Open Telemetry (optional)
const { ApolloOpenTelemetry } = require('supergraph-demo-opentelemetry');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, required: false },
  name: { type: String, required: false },
  totalProductsCreated: { type: Number, required: false },
}, { _id: false, collection: 'users' })

const User = mongoose.model('Users', userSchema);


if (process.env.APOLLO_OTEL_EXPORTER_TYPE) {
  new ApolloOpenTelemetry({
    type: 'subgraph',
    name: 'users',
    exporter: {
      type: process.env.APOLLO_OTEL_EXPORTER_TYPE, // console, zipkin, collector
      host: process.env.APOLLO_OTEL_EXPORTER_HOST,
      port: process.env.APOLLO_OTEL_EXPORTER_PORT,
    }
  }).setupInstrumentation();
}

const { ApolloServer, gql } = require('apollo-server');
const { buildSubgraphSchema } = require('@apollo/subgraph');
const { readFileSync } = require('fs');

const port = process.env.APOLLO_PORT;

const typeDefs = gql(readFileSync('./users.graphql', { encoding: 'utf-8' }));
const resolvers = {
  User: {
    __resolveReference: async (reference) => {
      const res = User.findOne({ "email": reference.email })
      return res != null ? res : new User({
        email: "Email",
        name: "Name",
        totalProductsCreated: 0,
      })
    }
  }
}
const server = new ApolloServer({ schema: buildSubgraphSchema({ typeDefs, resolvers }) });

mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@mongo:27017/${process.env.DB_NAME}`)
  .then(() => {
    console.log(`🚀 Users connected to DB`);
    server.listen({ port: port })
      .then(({ url }) => {
        console.log(`🚀 Users subgraph ready at ${url}`);
      })
      .catch(err => { console.error(err) });
  })
  .catch(err => {
    console.log(err)
  })