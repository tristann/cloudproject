print("==============================================================================================================")
print("========================================================INIT==================================================")
print("==============================================================================================================")

db = db.getSiblingDB('api_cloudlab');

db.createUser(
  {
    user: 'api_user',
    pwd: '3owfXNhNZXjR7An5',
    roles: [{ role: 'readWrite', db: 'api_cloudlab' }],
  },
);
db.createUser(
  {
    user: 'api_inventory',
    pwd: '6fXiCHbOmvW1dD3U',
    roles: [{ role: 'readWrite', db: 'api_cloudlab' }],
  },
);
db.createUser(
  {
    user: 'api_product',
    pwd: 'ck5u3SOICtrEI3L2',
    roles: [{ role: 'readWrite', db: 'api_cloudlab' }],
  },
);
db.createUser(
  {
    user: 'api_panda',
    pwd: 'QSBBjGEiomTbI2IU',
    roles: [{ role: 'readWrite', db: 'api_cloudlab' }],
  },
);

db.createCollection('users');
db.createCollection('inventory');
db.createCollection('pandas');
db.createCollection('products');

db.users.insertMany([
  { email: 'support@apollographql.com', name: "Apollo Studio Support", totalProductsCreated: 4 }
]);

db.inventory.insertMany([
  { id: 'apollo-federation', estimatedDelivery: '6/25/2021', fastestDelivery: '6/24/2021' },
  { id: 'apollo-studio', estimatedDelivery: '6/25/2021', fastestDelivery: '6/24/2021' },
]);

db.pandas.insertMany([
  { name: 'Basi', favoriteFood: "bamboo leaves" },
  { name: 'Yun', favoriteFood: "apple" }
]);

db.products.insertMany([
  { id: 'apollo-federation', sku: 'federation', package: '@apollo/federation', variation: "OSS" },
  { id: 'apollo-studio', sku: 'studio', package: '', variation: "platform" },
]);

print("==============================================================================================================")
print("========================================================INIT==================================================")
print("==============================================================================================================")