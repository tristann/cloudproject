import { useQuery, gql } from '@apollo/client';
import { Query } from '../models/models';

const GET_ALL_PRODUCTS = gql`
  query {
    allProducts {
      createdBy {
        email
        name
        totalProductsCreated
      }
      delivery {
        estimatedDelivery
        fastestDelivery
      }
      dimensions {
        size
        weight
      }
      id
      package
      sku
      variation {
        id
      }
    }
  }
`;

export function QueryProducts() {
    const { loading, error, data } = useQuery<Query>(GET_ALL_PRODUCTS);

    if(error !== undefined) {
      return {error};
    }

    return {loading, data};
}