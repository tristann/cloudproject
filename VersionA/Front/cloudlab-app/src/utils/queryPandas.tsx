import { useQuery, gql } from '@apollo/client';
import { Query } from '../models/models';

const GET_ALL_PANDAS = gql`
  query {
    allPandas {
      name
      favoriteFood
    }
  }
`;

export function QueryPandas() {
    const { loading, error, data } = useQuery<Query>(GET_ALL_PANDAS);

    if(error !== undefined) {
      return {error};
    }

    return {loading, data};
}