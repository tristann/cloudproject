import React from 'react';
import logo from './logo.svg';
import './App.css';
import { QueryPandas } from './utils/queryPandas';
import { PandasTable } from './components/pandasTableComponent';
import { QueryProducts } from './utils/queryProducts';
import { Col, ListGroup, Row, Tab } from 'react-bootstrap';

function App() {

  const pandas = QueryPandas();
  const products = QueryProducts();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Tab.Container id="list-group-tabs-pandas-products" defaultActiveKey="#pandas">
          <Row>
            <Col sm={4}>
              <ListGroup>
                <ListGroup.Item action href="#pandas">
                  List of pandas
                </ListGroup.Item>
                <ListGroup.Item action href="#products">
                  List of products
                </ListGroup.Item>
              </ListGroup>
            </Col>
            <Col sm={8}>
              <Tab.Content>
                <Tab.Pane eventKey="#pandas">
                  {pandas.error ? (
                    <p>{pandas.error.message}</p>
                  ): pandas.loading ? (
                    <p>Loading data...</p>
                  ): ( <PandasTable {...pandas.data?.allPandas}></PandasTable>)
                  }
                </Tab.Pane>
                <Tab.Pane eventKey="#products">
                  {products.error ? (
                    <p>{products.error.message}</p>
                  ): products.loading ? (
                    <p>Loading data...</p>
                  ): ( <PandasTable {...products.data?.allProducts}></PandasTable>)
                  }
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </header>
    </div>
  );
}

export default App;
