import React from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import { Panda, Product } from '../models/models';

export class PandaRow extends React.Component {
    object: Panda | Product;
    
    constructor(object: Panda | Product) {
        super(object);
        this.object = object;
    }

    isPanda(panda: Panda | Product): panda is Panda {
        return (panda as Panda).name !== undefined;
    }

    render() {
        const popover = 
        <Popover id={`popover-positioned-top`}>
            <Popover.Header as="h3">{`Delivery informations`}</Popover.Header>
            <Popover.Body>
                <strong>Estimated delivery: </strong> {this.isPanda(this.object) ? "" : this.object.delivery.estimatedDelivery} <br></br> <strong>Fastest delivery: </strong> {this.isPanda(this.object) ? "" : this.object.delivery.fastestDelivery}
            </Popover.Body>
        </Popover>

        const tds =
                this.isPanda(this.object) ? <><td>{this.object.name}</td><td>{this.object.favoriteFood}</td></> :
                <>
                    <td>{this.object.createdBy.name}</td>
                    <td>{this.object.createdBy.email}</td>
                    <td>{this.object.createdBy.totalProductsCreated}</td>
                    <td>{this.object.dimensions.size + " " + this.object.dimensions.weight}</td>
                    <td>{this.object.id}</td>
                    <td>{this.object.package}</td>
                    <td>{this.object.sku}</td>
                    <td>{this.object.variation.id}</td>
                </>

        return this.isPanda(this.object) ? <tr>{tds}</tr> :
            <OverlayTrigger {...'children="click"'} overlay={popover}>
                <tr>{tds}</tr>
            </OverlayTrigger>
    }
}