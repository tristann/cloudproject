import React from 'react';
import { Panda, Product } from '../models/models';
import { PandaRow } from './pandasTabsComponent';
import { Table } from 'react-bootstrap';

export class PandasTable extends React.Component {
    content: [Panda] | [Product];

    constructor(content: [Panda] | [Product]) {
        super(content);
        this.content = content;
    }

    isPanda(pandas: [Panda] | [Product]): pandas is [Panda] {
        return (pandas as [Panda])[0].name !== undefined;
    }

    render() {
        let array: (Panda|Product)[] = [];

        for(let i = 0; i < Object.keys(this.content).length; i++) {
            array.push(this.content[i]);
        }

        return (
            <Table striped hover variant="dark" size="sm">
                <thead>
                    <tr>
                        {this.isPanda(this.content) ? 
                            <>
                                <th>Panda name</th>
                                <th>Favorite food</th>
                            </> :
                            <>
                                <th>Created by</th>
                                <th>Mail</th>
                                <th>Product created</th>
                                <th>Dimensions</th>
                                <th>ID</th>
                                <th>Package</th>
                                <th>SKU</th>
                                <th>Variation ID</th>
                            </>
                        }
                    </tr>
                </thead>
                <tbody>
                    {array.map((object: Panda | Product) => {
                        return <PandaRow {...object} ></PandaRow>
                    })}
                </tbody>
            </Table>
        )
    }
}