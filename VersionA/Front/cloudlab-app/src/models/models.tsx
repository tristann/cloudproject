import React from 'react';

export interface DeliveryEstimates {
    estimatedDelivery: String
    fastestDelivery: String
}
  
export interface Panda {
    favoriteFood: String
    name: String
}

export interface Product {
    createdBy: User
    delivery: DeliveryEstimates
    dimensions: ProductDimension
    id: String
    package: String
    sku: String
    variation: ProductVariation
}

export interface ProductDimension {
    size: String
    weight: Number
}

export interface ProductVariation {
    id: String
}

export interface Query {
    allPandas: [Panda]
    allProducts: [Product]
    panda(name: String): Panda
    product(id: String): Product
}

export interface User {
    email: String
    name: String
    totalProductsCreated: Number
}